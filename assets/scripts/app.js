//
// app.js
// ------------------------------

// define our application and pull in ngRoute and ngAnimate
var spartanApp = angular.module('spartanApp', ['ngRoute', 'ngAnimate']);

// ROUTING ===============================================

// set our routing for this application
// each route will pull in a different controller
spartanApp.config(function($routeProvider, $locationProvider) {

    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'views/page-home.html',
            controller: 'homeController'
        })

        // projects page
        .when('/projects', {
            templateUrl: 'views/page-projects.html',
            controller: 'projectsController'
        })

        // work page
        .when('/work', {
            templateUrl: 'views/page-work.html',
            controller: 'workController'
        })

        // otherwise go home
        .otherwise({ redirectTo:'/' });

    // HTML5 History API
    $locationProvider.html5Mode(true);

});


// CONTROLLERS ============================================

// main controller
spartanApp.controller('mainController', function($scope) {
  $scope.date = new Date();
});

// home page controller
spartanApp.controller('homeController', function($scope) {
    $scope.pageClass = 'page-home';
});

// projects page controller
spartanApp.controller('projectsController', function($scope) {
    $scope.pageClass = 'page-projects';
});

// work page controller
spartanApp.controller('workController', function($scope) {
    $scope.pageClass = 'page-work';
});
